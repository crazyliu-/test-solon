# 测试Solon项目启动以及文件上传


---

### 1. 快速开始

- 项目环境：Git、JDK8、Maven
- 下载项目[当前项目](https://gitee.com/crazyliu-/test-solon.git)
- ~~在pom.xml中分别切换内置服务器jhttp、smarthttp、undertow、jetty，执行maven clean，然后运行StartTestApp，发现同样端口号只有smarthttp无法启动~~(要使用smarthttp，需要屏蔽solon.boot.jlhttp，这样就可以启动了)
- 在pom.xml中分别切换内置服务器jhttp、smarthttp、undertow、jetty，执行maven clean，然后运行StartTestApp，发现undertow启动后文件上传报错，jhttp、smarthttp、jetty正常

