package com.test;

import org.noear.solon.Solon;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Post;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.UploadedFile;
import org.noear.solon.extend.staticfiles.StaticMappings;
import org.noear.solon.extend.staticfiles.repository.FileStaticRepository;

@Controller
public class StartTestApp {

    public static void main(String[] args) {
        Solon.start(StartTestApp.class, args, app -> {

            String uploadPath = app.cfg().get("upload.path");
            //添加公共上传资源
            StaticMappings.add("/", new FileStaticRepository(uploadPath));
        });
    }

    @Post
    @Mapping(path = "/upload", multipart=true)
    public String upload(Context ctx) throws Exception {
        UploadedFile file = ctx.file("file");
        return file.name;
    }
}
